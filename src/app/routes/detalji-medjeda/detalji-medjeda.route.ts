import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MedjediModel } from '../../model/medjedi.model';

@Component({
  selector: 'detalji-medjeda',
  templateUrl: './detalji-medjeda.route.html',
  
})
export class DetaljiMedjedaComponent implements OnInit {

  medjed = {};

  constructor(private route:ActivatedRoute, public model:MedjediModel) { 
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getMedjedaById(id,(medjed)=>{
          this.medjed = medjed;
        });
      }
    );
  }

  ngOnInit() {
  }

}
