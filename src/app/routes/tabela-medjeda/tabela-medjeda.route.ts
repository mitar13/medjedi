import { Component, OnInit } from '@angular/core';
import { MedjediModel } from '../../model/medjedi.model';
import {config} from '../../config';
import { Router } from '@angular/router';
@Component({
  selector: 'tabela-medjeda',
  templateUrl: './tabela-medjeda.route.html',
  
})
export class TabelaMedjedaComponent implements OnInit {
  searchString = "";
  conf = config.medjediTableConfig;

  constructor(public model:MedjediModel, private router:Router) { }

  ngOnInit() {
  }

  obrisiMedjeda(id) {
    this.model.obrisiMedjeda(id);
  }

  
  
  
  editujMedjeda(id) {
    
   
    this.router.navigate(['medjed',id,'edit']);

  }

}
