import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'medjed',
  templateUrl: './medjed.route.html'
})
export class MedjedComponent implements OnInit {

  id;

  constructor(private router:Router,private route:ActivatedRoute ) {
    this.route.params.subscribe(({id})=>{
      this.id = id;
    })
  }

  goToPath(subpath){
    this.router.navigate(['/medjed',this.id,subpath])
  }

  ngOnInit() {
  }

}
