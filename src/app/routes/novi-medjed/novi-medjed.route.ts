import { Component, OnInit } from '@angular/core';
import { MedjediModel } from '../../model/medjedi.model';

@Component({
  selector: 'novi-medjed',
  templateUrl: './novi-medjed.route.html',
 
})
export class NoviMedjedComponent implements OnInit {

  constructor(public model:MedjediModel) { }
 noviMedjed={
   ime:null,
   kilaza:null,
   visina:null
 };
 dodajMedjeda(){
  
   if(this.noviMedjed.ime && this.noviMedjed.kilaza && this.noviMedjed.visina){
     this.model.dodajMedjeda(this.noviMedjed)
   }
   
 }

 ngOnInit() {
 }
 
}