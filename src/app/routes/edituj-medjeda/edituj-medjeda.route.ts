import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MedjediModel } from '../../model/medjedi.model';


@Component({
  selector: 'edituj-medjeda',
  templateUrl: './edituj-medjeda.route.html'
})
export class EditujMedjedaComponent implements OnInit {

  editovaniMedjed = {};

  constructor(private route:ActivatedRoute,public model:MedjediModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getMedjedaById(id,(medjed)=>{
          this.editovaniMedjed = medjed;
        });
      }
    );
  }

  updejtujMedjeda(){
    this.model.updejtujMedjeda(this.editovaniMedjed);
  }

  ngOnInit() {
  }

}

