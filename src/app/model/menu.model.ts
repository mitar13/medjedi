import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
 
export class MenuModel {


  constructor(private router:Router) {}

  linkovi = [
    {putanja:"/",naziv:"Kuca"},
    {putanja:"/dashboard",naziv:"Tabela medjeda"},
    {putanja:"/medjedi/novi",naziv:"Dodaj medjeda"}
  ]

  goToPath(path){
    let pathArray = [];
    pathArray.push(path);
    this.router.navigate(pathArray);
  }
}
