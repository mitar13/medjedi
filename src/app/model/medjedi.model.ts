import { Injectable } from '@angular/core';
import { MedjediService } from '../services/medjedi.service';
import { Router } from '@angular/router';

@Injectable()
export class MedjediModel {

    medjedi = [];

    constructor(private service: MedjediService, private router: Router) {
        this.refreshujMedjede(()=>{});
    }

    refreshujMedjede(clbk) {
        this.service.getAll().subscribe(
            (medjedi) => {
                this.medjedi = medjedi;
                clbk()
            }
        );
    }

    updejtujMedjeda(medjed){
        this.service.update(medjed).subscribe((updejtovaniMedjed)=>{
            this.refreshujMedjede(()=>{})
            this.router.navigate(["dashboard"]);
           
        })
    }

    getMedjedaById(id, clbk){
        this.service.getById(id).subscribe(clbk);
    }

    getMedjedaByIdCallback(id, clbk){
        if(this.medjedi.length < 1){
            this.refreshujMedjede(()=>{
                for (let i = 0; i < this.medjedi.length; i++) {
                    if (id == this.medjedi[i].id) {
                        clbk(this.medjedi[i])
                    }
                }
            })
        }else{
            for (let i = 0; i < this.medjedi.length; i++) {
                if (id == this.medjedi[i].id) {
                    clbk(this.medjedi[i])
                }
            }
        }
        
    }

    dodajMedjeda(medjed) {
        this.service.post(medjed).subscribe(
            (medjed) => {
                this.medjedi.push(medjed);
                this.refreshujMedjede(()=>{});
            }
            
        )
        
        this.router.navigate(["dashboard"]);
        
        
                
    }

    dajMiIndexMedjedaPoIdju(id,clbk) {
        for (let i = 0; i < this.medjedi.length; i++) {
            if (id == this.medjedi[i].id) {
                clbk(i)
            }
        }
    }

    obrisiMedjeda(id) {
        this.service.delete(id).subscribe(() => {
           this.dajMiIndexMedjedaPoIdju(id,(index)=>{
                this.medjedi.splice(index,1);
            });
            this.refreshujMedjede(()=>{});
        });
    }

}