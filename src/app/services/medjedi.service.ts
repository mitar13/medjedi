import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config';
import "rxjs/add/operator/map";

@Injectable()
export class MedjediService {

   constructor(private http: Http) { }

   getAll() {
       return this.http
           .get(config.apiUrl + '/medjedi')
           .map((response) => { return response.json() });
   }
   getById(id) {
       return this.http
           .get(config.apiUrl + '/medjedi/'+id)
           .map((response) => { return response.json() });
   }

   post(medjed) {
       return this.http
           .post(config.apiUrl + '/medjedi',medjed)
           .map((response) => { return response.json() });
   }
   update(medjed) {
       return this.http
           .patch(config.apiUrl + '/medjedi/'+medjed.id, medjed)
           .map((response) => { return response.json() });
   }
   delete(id) {
       return this.http
           .delete(config.apiUrl + '/medjedi/'+id)
           .map((response) => { return response.json() });
   }
}