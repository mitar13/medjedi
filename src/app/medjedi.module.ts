import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import {HttpModule} from '@angular/http'
import {FormsModule} from '@angular/forms'
import { MyDatePickerModule } from '../../node_modules/angular4-datepicker/src/my-date-picker';

import { MedjediComponent } from './medjedi.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';

import { MenuModel } from './model/menu.model';
import { HomeComponent } from './routes/home/home.route';
import { TabelaMedjedaComponent } from './routes/tabela-medjeda/tabela-medjeda.route';
import { NoviMedjedComponent } from './routes/novi-medjed/novi-medjed.route';
import { EditujMedjedaComponent } from './routes/edituj-medjeda/edituj-medjeda.route';
import { DetaljiMedjedaComponent } from './routes/detalji-medjeda/detalji-medjeda.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';
import { MedjediModel } from './model/medjedi.model';
import { MedjediService } from './services/medjedi.service';
import { FilterPipe } from './pipes/filter.pipe';
import { MedjedComponent } from './routes/medjed/medjed.route';


const routes:Routes = [
  {path:'',component:HomeComponent},
  {path:'dashboard',component:TabelaMedjedaComponent},
  {path:'medjedi/novi',component:NoviMedjedComponent},
  {
    path: "medjed/:id", component: MedjedComponent, children: [
      { path: "edit", component: EditujMedjedaComponent },
      { path: "info", component: DetaljiMedjedaComponent },
    ]
  },
]

@NgModule({
  declarations: [
    MedjediComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TabelaMedjedaComponent,
    NoviMedjedComponent,
    EditujMedjedaComponent,
    DetaljiMedjedaComponent,
    MenuLinkComponent,
    FilterPipe,
    MedjedComponent,
    AngularDateTimePickerModule

  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    FormsModule
    
  ],
  providers: [
    MenuModel,
    MedjediModel,
    MedjediService
  ],
  bootstrap: [MedjediComponent]
})
export class MedjediModule { }
